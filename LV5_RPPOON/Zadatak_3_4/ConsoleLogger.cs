﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_3_4
{
    class ConsoleLogger
    {
        static ConsoleLogger instance;

        private ConsoleLogger(){ }

        public static ConsoleLogger GetInstance()
        {
            if(instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }

        public void Log()
        {
            Console.WriteLine("{0}: Logged in dataset!", DateTime.Now);
        }
    }
}
