﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_3_4
{
    class Example_3: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            IDataset virtualProxy = new VirtualProxyDataset("C:\\Users\\Vlatka\\source\\repos\\LV5_RPPOON\\LV5_RPPOON\\bin\\Debug\\netcoreapp3.1\\Dataset.txt");
            User user1 = User.GenerateUser("Vlatka");
            IDataset protectionProxyDataset = new ProtectionProxyDataset(user1);
            
            DataConsolePrinter printer = new DataConsolePrinter();

            User user2 = User.GenerateUser("Vova");
            IDataset protectionProxy = new ProtectionProxyDataset(user2);

            Console.WriteLine("Virtual proxy working:");
            printer.ConsolePrintData(virtualProxy);

            Console.WriteLine("Protection proxy working:");

            Console.WriteLine("First login:");
            printer.ConsolePrintData(protectionProxyDataset);
            Console.WriteLine("Second login:");
            printer.ConsolePrintData(protectionProxy);
        }
    }
}
