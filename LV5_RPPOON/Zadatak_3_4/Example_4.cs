﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LV5_RPPOON.Zadatak_3_4
{
    class Example_4: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            IDataset loggingProxyDataset = new LoggingProxyDataset("C:\\Users\\Vlatka\\source\\repos\\LV5_RPPOON\\LV5_RPPOON\\bin\\Debug\\netcoreapp3.1\\Dataset.txt");
            DataConsolePrinter printer = new DataConsolePrinter();

            ReadOnlyCollection<List<string>> data = loggingProxyDataset.GetData();
            System.Threading.Thread.Sleep(2000);
            printer.ConsolePrintData(loggingProxyDataset);

        }
    }
}
