﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace LV5_RPPOON.Zadatak_3_4
{
    class DataConsolePrinter
    {
        public DataConsolePrinter() { }
        public void ConsolePrintData(IDataset dataset)
        {
            ReadOnlyCollection<List<string>> tempDataset = dataset.GetData();

            if (tempDataset == null)
            {
                Console.WriteLine("You don't have a permission to access this data!");
            }
            else
            {
                foreach (List<string> data in tempDataset)
                {
                    foreach (string item in data)
                    {
                        Console.Write(item + " ");
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
