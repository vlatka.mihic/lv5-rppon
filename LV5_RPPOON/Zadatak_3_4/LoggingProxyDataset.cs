﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace LV5_RPPOON.Zadatak_3_4
{
    class LoggingProxyDataset: IDataset
    {
        private Dataset dataset;
        private string filePath;

        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            ConsoleLogger.GetInstance().Log();
            return dataset.GetData();
        }
    }
}
