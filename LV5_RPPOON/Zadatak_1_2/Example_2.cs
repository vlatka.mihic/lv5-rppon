﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_1_2
{
    class Example_2: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }

        public void Run()
        {
            List<IShipable> shipable = new List<IShipable>();
            Product book = new Product("Book: Head First Design Patterns", 89.99, 0.7);
            Product pencil = new Product("Black pencil", 7.99, 0.05);
            Box schoolSupplies = new Box("School Supplies");
            schoolSupplies.Add(book);
            schoolSupplies.Add(pencil);

            Box hygiene = new Box("Bathroom stuff");
            hygiene.Add(new Product("Soup", 4.99, 0.3));
            hygiene.Add(new Product("Toothbrush", 25.99, 0.1));
            hygiene.Add(new Product("Toothpast", 18.57, 0.075));

            Box coronaDelivery = new Box("We're all in this together");
            coronaDelivery.Add(schoolSupplies);
            coronaDelivery.Add(hygiene);

            double totalPrice = coronaDelivery.Price;
            double totalWeight = coronaDelivery.Weight;
            ShippingService shipping = new ShippingService(2.34);
            double totalShipping = shipping.GetPrice(coronaDelivery);

            Console.WriteLine(coronaDelivery.Description());
            Console.WriteLine("Total product price: {0}kn \nTotal weight: {1}kg \n", Math.Round(totalPrice, 3), Math.Round(totalWeight, 3));
            Console.WriteLine("Total shipping price: {0}kn \nTotal price: {1}kn", Math.Round(totalShipping, 3), Math.Round(totalPrice + totalShipping, 3));
        }
    }
}
