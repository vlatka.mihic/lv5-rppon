﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_1_2
{
    class ShippingService
    {
        public double pricePerKilo { private get; set; }
        public ShippingService(double pricePerKilo) { this.pricePerKilo = pricePerKilo; }

        public double GetPrice(IShipable item)
        {
            return item.Weight * pricePerKilo;
        }
    }
}
