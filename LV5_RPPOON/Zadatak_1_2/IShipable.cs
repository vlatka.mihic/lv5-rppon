﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_1_2
{
    interface IShipable
    {
        double Price { get; }
        double Weight { get; }
        string Description(int depth = 0);

    }
}
