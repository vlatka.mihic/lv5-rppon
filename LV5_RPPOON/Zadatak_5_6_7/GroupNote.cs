﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_5_6_7
{
    class GroupNote: Note
    {
        List<string> Group;
        public GroupNote(string message, ITheme theme) : base(message, theme) 
        {
            this.Group = new List<string>();
        }
        public void AddName(string name)
        {
            this.Group.Add(name);
        }
        public void RemoveName(string name)
        {
            if (this.Group.Contains(name))
            {
                this.Group.RemoveAt(this.Group.IndexOf(name));
            }
        }
        private string MessageWithGroup(string framedMessage)
        {
            StringBuilder builder = new StringBuilder();
            string[] pieces = framedMessage.Split('\n');
            builder.Append(pieces[0] + '\n' + pieces[1] + '\n');
            foreach (string name in this.Group)
            {
                builder.Append(name + '\n');
            }
            builder.Append(pieces[2]);
            return builder.ToString(); 
        }
        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("Message refers to the group of people below: ");
            string framedMessage = this.GetFramedMessage();
            string wholeMessage = this.MessageWithGroup(framedMessage);
            Console.WriteLine(wholeMessage);
            Console.ResetColor();
        }
    }
}
