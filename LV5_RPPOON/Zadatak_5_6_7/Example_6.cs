﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_5_6_7
{
    class Example_6: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            ITheme theme = new LightTheme();
            GroupNote note = new GroupNote("Hello world!", theme);

            note.AddName("Vlatka");
            note.AddName("Ivan");
            note.AddName("Ana-Marija");
            note.AddName("Josip");

            note.Show();

            ITheme comment = new CommentTheme();
            note.Theme = comment;

            note.RemoveName("Vlatka");
            note.RemoveName("Darko");
            note.Show();
        }
    }
}
