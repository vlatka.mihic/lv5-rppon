﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_5_6_7
{
    class CommentTheme: ITheme
    {
        public void SetBackgroundColor()
        {
            Console.BackgroundColor = ConsoleColor.Black;
        }
        public void SetFontColor()
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }
        public string GetHeader(int width)
        {
            return "/" + new string('*', width-1);
        }
        public string GetFooter(int width)
        {
            return new string('*', width-1) + "/";
        }
    }
}
