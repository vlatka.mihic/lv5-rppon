﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_5_6_7
{
    class Example_7: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            /*---Before change---
             
            ITheme theme = new LightTheme();
            GroupNote note1 = new GroupNote("Finish your laboratory exercises till wednesday(8pm)!", theme);

            note1.AddName("Vlatka");
            note1.AddName("Ivan");
            note1.AddName("Ana-Marija");
            note1.AddName("Josip");

            Note note2 = new ReminderNote("Do the dishes!", theme);
            Note note3 = new ReminderNote("#stayhome \n#staysafe", theme);
            Notebook notebook = new Notebook();

            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);

            notebook.Display();

            ITheme comment = new CommentTheme();
            note1.Theme = comment;
            note2.Theme = comment;
            note3.Theme = comment;

            note1.RemoveName("Vlatka");
            note1.RemoveName("Darko");

            notebook.Display();
            */

            /*---After change---*/

            ITheme theme = new LightTheme();
            ITheme comment = new CommentTheme();
            GroupNote note1 = new GroupNote("Finish your laboratory exercises till wednesday(8pm)!", theme);

            note1.AddName("Vlatka");
            note1.AddName("Ivan");
            note1.AddName("Ana-Marija");
            note1.AddName("Josip");

            Note note2 = new ReminderNote("Do the dishes!", comment);
            Note note3 = new ReminderNote("#stayhome \n#staysafe", theme);
            Notebook notebook = new Notebook(theme);

            notebook.AddNote(note1);
            notebook.AddNote(note2);
            notebook.AddNote(note3);

            notebook.Display();

            notebook.ChangeTheme(comment);

            notebook.Display();

        }
    }
}
