﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPOON.Zadatak_5_6_7
{
    class Example_5: IExample
    {
        public string Name
        {
            get
            {
                return this.ToString();
            }
        }
        public void Run()
        {
            ITheme theme = new LightTheme();
            Note note = new ReminderNote("Hello world!", theme);

            note.Show();

            ITheme comment = new CommentTheme();
            note.Theme = comment;

            note.Show();
        }
    }
}
